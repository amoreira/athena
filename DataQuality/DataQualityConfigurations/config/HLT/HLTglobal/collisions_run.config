# **********************************************************************
# $Id: collisions_run.config 
# **********************************************************************

#######################
# HLT
#######################

#######################
# Output
#######################

output top_level {
    output HLT {

       #Run3 folders 
       output Run3 {
          output TRHLT {
             output AllChains {
             }
             output Electrons {
             }
             output Gamma {
             }
             output Muons {
             }
             output MinBias {
             }
             output MissingET {
             }
             output Taus {
             }
             output Jets {
             }
          #end TRHLT
          }
       #end Run3
       }

       #Run2 folders
       output Run2 {
          output TRHLT {
             output AllChains {
             }
             output Electrons {
             }
             output Gamma {
             }
             output Muons {
             }
##           output MinBias {
##           }
             output MissingET {
             }
             output Taus {
             }
             output Jets {
             }
          #end TRHLT
          }
       #end Run2
       }
    #end HLT
    }
#end top_level
}

##############
# References
##############

reference HLT_local_reference {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/
  file = data15_13TeV.00267638.physics_EnhancedBias.merge.HIST.r6857_p1831.root
  path = run_267638
  name = same_name
}


#######################
# Histogram Assessments
#######################

#### Run3

dir HLT {
   #Put Run3 folders and histograms here
   #output = HLT/Run3/TRHLT
   dir ResultMon {
       #output = HLT/Run3/TRHLT
       
       hist ConfigConsistency_HLT {
         algorithm = HLT_Histogram_Empty
         description = All bins should be empty. If any bin is filled, inform the Trigger ONLINE Expert IMMEDIATELY. 
	 output = HLT/Run3/TRHLT
       }

       hist HLTEvents { 
	 algorithm = HLT_Histogram_Not_Empty&GatherData
	 output = HLT/Run3/TRHLT
       }

       hist L1Events { 
         algorithm = HLT_Histogram_Not_Empty&GatherData
	 output = HLT/Run3/TRHLT
       }

       dir AllChains {
 	#output = HLT/Run3/TRHLT/AllChains
        hist HLT_AllChainsPS {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/AllChains
	}
	hist HLT_AllChainsRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/AllChains
	}
	hist AllChainsRoIs {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert on-call. 
	  output = HLT/Run3/TRHLT/AllChains
	}
       }

       dir Electrons {
 	#output = HLT/Run3/TRHLT/Electrons
        hist HLT_ElectronsPS {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Electrons
	}
	hist HLT_ElectronsRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Electrons
	}
	hist ElectronsRoIs {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Egamma Expert on-call. 
	  output = HLT/Run3/TRHLT/Electrons
	}
       }

      dir Gamma {
 	#output = HLT/Run3/TRHLT/Gamma        
	hist HLT_GammaPS {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Gamma 
	}
	hist HLT_GammaRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Gamma 
	}
	hist GammaRoIs {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Egamma Expert on-call. 
	  output = HLT/Run3/TRHLT/Gamma 
	}
       }

       dir Jets {
 	#output = HLT/Run3/TRHLT/Jets
        hist HLT_JetsPS {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Jets
	}
	hist HLT_JetsRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Jets
	}
	hist JetsRoIs {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Jet/MET Expert on-call. 
	  output = HLT/Run3/TRHLT/Jets
	}
       }

       dir MissingET {
 	#output = HLT/Run3/TRHLT/MissingET
        hist HLT_MissingETPS { 
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/MissingET
	}
	hist HLT_MissingETRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/MissingET
	}
	hist MissingETRoIs {
	  regex = 1
	  algorithm = HLT_PassInput
	  output = HLT/Run3/TRHLT/MissingET
	}
       }

       dir Muons {
 	#output = HLT/Run3/TRHLT/Muons
	hist HLT_MuonsPS {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Muons
	}
	hist HLT_MuonsRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Muons
	}
	hist MuonsRoIs {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Muon Expert on-call. 
	  output = HLT/Run3/TRHLT/Muons
	}
       }

       dir Taus {
 	#output = HLT/Run3/TRHLT/Taus
        hist HLT_TausPS {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Taus
	}
	hist HLT_TausRAW {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run3/TRHLT/Taus
	}
	hist TausRoIs {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Tau Expert on-call. 
	  output = HLT/Run3/TRHLT/Taus
	}
       }

   #end dir ResultMon
   }
#end dir HLT
}

#### Run2
dir HLT {

   dir ResultMon {
       #output = HLT/Run2/TRHLT
       hist ConfigConsistency_HLT@RunTwo {
         algorithm = HLT_Histogram_Empty
         description = All bins should be empty. If any bin is filled, inform the Trigger ONLINE Expert IMMEDIATELY. 
	 output = HLT/Run2/TRHLT
       }

       hist HLTResultHLT@RunTwo { 
	 algorithm = HLT_Histogram_Not_Empty&GatherData
	 output = HLT/Run2/TRHLT
       }

       hist L1Events@RunTwo { 
         algorithm = HLT_Histogram_Not_Empty&GatherData
	 output = HLT/Run2/TRHLT
       }

		
       dir AllChains {
 	#output = HLT/Run2/TRHLT/AllChains
        hist HLT_AllChainsPS@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/AllChains
	}
        hist HLT_AllChainsPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/AllChains
	}
	hist HLT_AllChainsRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/AllChains
	}
	hist AllChainsRoIs@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert on-call. 
	  output = HLT/Run2/TRHLT/AllChains
	}
       }

       dir Electrons {
 	#output = HLT/Run2/TRHLT/Electrons
        hist HLT_ElectronsPS@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Electrons
	}
        hist HLT_ElectronsPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Electrons
	}
	hist HLT_ElectronsRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Electrons
	}
	hist ElectronsRoIs@RunTwo {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Egamma Expert on-call. 
	  output = HLT/Run2/TRHLT/Electrons
	}
       }

       dir Gamma {
 	#output = HLT/Run2/TRHLT/Gamma        
	hist HLT_GammaPS@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Gamma 
	}
	hist HLT_GammaPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Gamma 
	}
	hist HLT_GammaRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Gamma 
	}
	hist GammaRoIs@RunTwo {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Egamma Expert on-call. 
	  output = HLT/Run2/TRHLT/Gamma 
	}
       }

       dir Jets {
 	#output = HLT/Run2/TRHLT/Jets
        hist HLT_JetsPS@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Jets
	}
        hist HLT_JetsPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Jets
	}
	hist HLT_JetsRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Jets
	}
##	hist JetsRoIs@RunTwo {
##	  regex = 1
##          algorithm = HLT_Histogram_Not_Empty&GatherData
##          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Jet/MET Expert on-call. 
##          output = HLT/Run2/TRHLT/Jets
##	}
       }

##       dir MinBias {
## 	#output = HLT/Run2/TRHLT/MinBias
##        hist HLT.*P[ST] {
##	  regex = 1
##	  algorithm = HLT_PassInput
##	  output = HLT/Run2/TRHLT/MinBias
##	}
##	hist HLT_MinBiasRAW {
##	  regex = 1
##	  algorithm = HLT_PassInput
##	  output = HLT/Run2/TRHLT/MinBias
##	}
##	hist .*RoIs {			# have RoIs just pass for now since they are empty
##	  regex = 1
##	  algorithm = HLT_PassInput
##	  output = HLT/Run2/TRHLT/MinBias
##	}
##       }

       dir MissingET {
 	#output = HLT/Run2/TRHLT/MissingET
        hist HLT_MissingETPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/MissingET
	}
##        hist HLT_MissingETPS@RunTwo { ##Histo broken, known issue, masked for now.
##	  regex = 1
##        algorithm = HLT_Histogram_Not_Empty&GatherData
##	  output = HLT/Run2/TRHLT/MissingET
##	}
	hist HLT_MissingETRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/MissingET
	}
##	hist MissingETRoIs@RunTwo {
##	  regex = 1
##	  algorithm = HLT_PassInput
##	  output = HLT/Run2/TRHLT/MissingET
##	}
       }

       dir Muons {
 	#output = HLT/Run2/TRHLT/Muons

	hist HLT_MuonsPS@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Muons
	}
	hist HLT_MuonsPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Muons
	}
	hist HLT_MuonsRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Muons
	}
	hist MuonsRoIs@RunTwo {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Muon Expert on-call. 
	  output = HLT/Run2/TRHLT/Muons
	}
       }

       dir Taus {
 	#output = HLT/Run2/TRHLT/Taus
        hist HLT_TausPS@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Taus
	}
        hist HLT_TausPT@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Taus
	}
	hist HLT_TausRAW@RunTwo {
	  regex = 1
          algorithm = HLT_Histogram_Not_Empty&GatherData
	  output = HLT/Run2/TRHLT/Taus
	}
	hist TausRoIs@RunTwo {
	  regex = 1
	  algorithm = HLT_Histogram_Not_Empty&GatherData
          description = Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Tau Expert on-call. 
	  output = HLT/Run2/TRHLT/Taus
	}
       }


   }
}


##############
# Algorithms
##############

compositeAlgorithm HLT_Histogram_Not_Empty&GatherData {
  subalgs = GatherData,Histogram_Not_Empty
  libnames = libdqm_algorithms.so
}

algorithm HLT_Histogram_Not_Empty&GatherData {
  name = HLT_Histogram_Not_Empty&GatherData
}

algorithm HLT_Histogram_Not_Empty_with_Ref&GatherData {
  name = HLT_Histogram_Not_Empty&GatherData
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
}


algorithm HLT_PassInput {
  libname = libdqm_algorithms.so
  name = PassInput
}

algorithm HLT_Histogram_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Empty
}




###############
# Thresholds
###############
