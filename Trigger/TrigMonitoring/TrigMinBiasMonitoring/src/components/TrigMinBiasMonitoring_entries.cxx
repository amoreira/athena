#include "../HLTMinBiasMonTool.h"
#include "../HLTMinBiasTrkMonAlg.h"
#include "../HLTMBTSMonitoringAlgMT.h"
#include "../HLTMinBiasEffMonitoringAlg.h"
#include "../TrigAFPSidHypoMonitoringAlg.h"

DECLARE_COMPONENT( HLTMinBiasMonTool )
DECLARE_COMPONENT( HLTMinBiasTrkMonAlg )
DECLARE_COMPONENT( HLTMBTSMonitoringAlgMT )
DECLARE_COMPONENT( HLTMinBiasEffMonitoringAlg )
DECLARE_COMPONENT( TrigAFPSidHypoMonitoringAlg )
